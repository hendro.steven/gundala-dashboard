<?php

class RestrictedController extends Controller {

    protected $session;
    private $sessionSvr;

    public function __construct() {
        parent::__construct();
        $this->sessionSvr = new SessionServices();
        $this->session = $this->f3->get('SESSION.acc');
    }

    function beforeroute() {
        if (!$this->f3->exists('SESSION.acc')) {
            $this->f3->reroute('/');
        }else{
            $sessionId = $this->session->sessionId;
            $currSession = $this->sessionSvr->findOne($this->session->sessionId);
            if(!$currSession->data->valid){
                $this->f3->clear('SESSION');
                $this->f3->reroute('/');
            }
        }
        
    }

    function afterroute() {
        echo Template::instance()->render('layout.html');
        $this->f3->clear('SESSION.flash');
    }

}
