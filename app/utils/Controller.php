<?php

class Controller {

    protected $f3;
    protected $db;
    protected $model;
    protected $table;
    protected $logger;
    protected $responseJson;

    function __construct() {
        $this->f3 = Base::instance();
        //db connection
        //$this->db = new DB\Mongo($this->f3->get('db_host'), $this->f3->get('db_name'));
        //init logger
        $this->logger = new Log("logs/". date("Y-m-d") . ".log"); 
        //responseJson
        $this->responseJson = array();
    }

}
