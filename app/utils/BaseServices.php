<?php

class BaseServices {

    protected $url;
    protected $token;
    protected $f3;
    protected $logger;

    public function __construct() {
        $this->f3 =  Base::instance();
        $this->logger = new Log("logs/". date("Y-m-d") . ".log"); 
        $this->url = $this->f3->get('API_HOST');
        $this->token = $this->f3->get('API_TOKEN');
    }
    
    public function post($path, $data){
        $path = "$this->url$path";
        $response = \Httpful\Request::post($path)                
            ->sendsJson()       
            ->addHeaders(array(
                'Content-Type' => 'application/json',              
                'Authorization' => "BEARER $this->token",              
             ))                
            ->body($data)             
            ->send();  
        return json_decode(json_encode($response->body));
    }

    public function get($path, $data=null) {
        $path = "$this->url$path";
        $response = \Httpful\Request::get($path)                
            ->sendsJson()       
            ->addHeaders(array(
                'Content-Type' => 'application/json',              
                'Authorization' => "BEARER $this->token",              
             ))                
            ->body($data)             
            ->send();  
        return json_decode(json_encode($response->body));
    }

    public function put($path, $data){
        $path = "$this->url$path";
        $response = \Httpful\Request::put($path)                
            ->sendsJson()       
            ->addHeaders(array(
                'Content-Type' => 'application/json',              
                'Authorization' => "BEARER $this->token",              
            ))                
            ->body($data)             
            ->send();  
        return json_decode(json_encode($response->body));
    }


}