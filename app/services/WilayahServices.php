<?php

class WilayahServices extends BaseServices{

    public function __construct() {
        parent::__construct();
    }

    public function findAll($sessionId=null, $page=1, $limit=100){
        $data = array(
            "sessionId" => $sessionId
        );
        $result = $this->get("/wilayah/$page/$limit", json_encode($data));
        $listWilayah = $result->data->subset;
        return json_encode($listWilayah);
    }

    public function createOne($sessionId=null, $name){
        $data = array(
            "sessionId" => $sessionId,
            "nama" => $name
        );
        $result = $this->post("/wilayah/create", json_encode($data));
        return json_encode($result);
    }

    public function findOne($sessionId, $id){
        $data = array(
            "sessionId" => $sessionId
        );
        $result = $this->post("/wilayah/$id", json_encode($data));
        return json_encode($result->data);
    }

}