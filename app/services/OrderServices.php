<?php

class OrderServices extends BaseServices{

    public function __construct() {
        parent::__construct();
    }

    public function findAll($sessionId=null, $page=1, $limit=500){
        $data = array(
            "sessionId" => $sessionId
        );
        $result = $this->get("/order/all/$page/$limit", json_encode($data));
        $listData = $result->data->subset;
        return json_encode($listData);
    }

    public function findOne($sessionId=null, $id){
        $data = array(
            "sessionId" => $sessionId
        );
        $result = $this->get("/order/find/$id", json_encode($data));
        $app = $result->data;
        return $app;
    }

    public function updateSurveyor($sessionId=null, $id, $surveyor){
        $data = array(
            "sessionId" => $sessionId
        );
        $result = $this->get("/order/find/$id", json_encode($data));
        $app = $result->data;
        
        $data = array(
            "sessionId" => $sessionId,
            "survey_by" => $surveyor,
	        "customer" => $app->customer_name,
	        "telp" => $app->customer_phone,
	        "address" => $app->customer_address,
	        "source" => $app->source,
	        "jenis" => $app->jenis_motor
        );
        $result = $this->put("/order/update/$id", json_encode($data));
        return $result;
    }

    public function updateStatus($sessionId=null, $id, $updateBy, $status){
        $data = array(
            "sessionId" => $sessionId,
            "updated_by" => $updateBy,
	        "status" => $status
        );
        $result = $this->put("/order/status/$id", json_encode($data));
        return $result;
    }

    public function getImages($id){
        return json_encode($this->get("/upload/order/$id", null));
    }

}