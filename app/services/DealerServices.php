<?php

class DealerServices extends BaseServices{

    public function __construct() {
        parent::__construct();
    }

    public function findAll($sessionId=null, $page=1, $limit=100){
        $data = array(
            "sessionId" => $sessionId
        );
        $result = $this->get("/dealer/$page/$limit", json_encode($data));
        $listData = $result->data->subset;
        return json_encode($listData);
    }

    public function createOne($sessionId=null, $email, $phone, $password, $fullName, $namaDealer, $alamat){
        $data = array(
            "sessionId" => $sessionId,
            "email" => $email,
            "phone" => $phone,
            "password" => $password,
            "full_name" => $fullName,          
            "nama_dealer" => $namaDealer,
            "alamat" => $alamat           
        );
        $result = $this->post("/dealer/create", json_encode($data));
        return $result;
    }

    public function findOne($sessionId, $id){
        $data = array(
            "sessionId" => $sessionId
        );
        $result = $this->post("/surveyor/$id", json_encode($data));
        return json_encode($result->data);
    }

}