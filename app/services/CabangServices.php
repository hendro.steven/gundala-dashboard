<?php

class CabangServices extends BaseServices{

    public function __construct() {
        parent::__construct();
    }

    public function findAll($sessionId=null, $page=1, $limit=100){
        $data = array(
            "sessionId" => $sessionId
        );
        $result = $this->get("/cabang/$page/$limit", json_encode($data));
        $listCab = $result->data->subset;
        return json_encode($listCab);
    }

    public function createOne($sessionId=null, $name, $area){
        $data = array(
            "sessionId" => $sessionId,
            "nama" => $name,
            "area" => $area
        );
        $result = $this->post("/cabang/create", json_encode($data));
        return $result;
    }

    public function findOne($sessionId, $id){
        $data = array(
            "sessionId" => $sessionId
        );
        $result = $this->post("/cabang/$id", json_encode($data));
        return json_encode($result->data);
    }

}