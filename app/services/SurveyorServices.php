<?php

class SurveyorServices extends BaseServices{

    public function __construct() {
        parent::__construct();
    }

    public function findAll($sessionId=null, $page=1, $limit=1000){
        $data = array(
            "sessionId" => $sessionId
        );
        $result = $this->get("/surveyor/$page/$limit", json_encode($data));
        $listData = $result->data->subset;
        return json_encode($listData);
    }

    public function createOne($sessionId=null, $email, $phone, $password, $fullName, $cabang){
        $data = array(
            "sessionId" => $sessionId,
            "email" => $email,
            "phone" => $phone,
            "password" => $password,
            "full_name" => $fullName,
            "cabang" => $cabang
        );
        $result = $this->post("/surveyor/create", json_encode($data));
        return $result;
    }

    public function findOne($sessionId, $id){
        $data = array(
            "sessionId" => $sessionId
        );
        $result = $this->post("/surveyor/$id", json_encode($data));
        return json_encode($result->data);
    }

}