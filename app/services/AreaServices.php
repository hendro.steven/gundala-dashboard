<?php

class AreaServices extends BaseServices{

    public function __construct() {
        parent::__construct();
    }

    public function findAll($sessionId=null, $page=1, $limit=100){
        $data = array(
            "sessionId" => $sessionId
        );
        $result = $this->get("/area/$page/$limit", json_encode($data));
        $listArea = $result->data->subset;
        return json_encode($listArea);
    }

    public function createOne($sessionId=null, $name, $wilayah){
        $data = array(
            "sessionId" => $sessionId,
            "nama" => $name,
            "wilayah" => $wilayah
        );
        $result = $this->post("/area/create", json_encode($data));
        return json_encode($result);
    }

    public function findOne($sessionId, $id){
        $data = array(
            "sessionId" => $sessionId
        );
        $result = $this->post("/area/$id", json_encode($data));
        return json_encode($result->data);
    }

}