<?php


class CabangController extends RestrictedController {

    private $cabangSvr;
    private $areaSvr;

    public function __construct() {
        parent::__construct(); 
        $this->areaSvr = new AreaServices();
        $this->cabangSvr = new CabangServices();
    }

    public function index() {
        $listCabang = $this->cabangSvr->findAll($this->session->sessionId);
        $this->f3->set('listCabang',json_decode($listCabang,true));
        $this->f3->set('view', 'cabang/list.html');
    }

    public function input() {
        $listArea = $this->areaSvr->findAll($this->session->sessionId);
        $this->f3->set('listArea',json_decode($listArea,true));
        $this->f3->set('view', 'cabang/input.html');    
    }

    public function save(){
        $name = $this->f3->get('POST.cabangName');
        $area = $this->f3->get('POST.area');

        $v = new Valitron\Validator(array('Cabang Name' => $name));
        $v->rule('required', ['Cabang Name']);

        if ($v->validate()) {
            try {
                $result = $this->cabangSvr->createOne($this->session->sessionId, $name, $area);
                $this->logger->write(json_encode($result));
                if($result->status){
                    $flash = array(
                        'errorType' => 'Success',
                        'infos' => array(array('Cabang saved'))
                    );
                    $this->f3->set('SESSION.flash', $flash);
                    $this->f3->reroute('/cabang');
                }else{
                    $flash = array(
                        'errorType' => 'Error(s)',
                        'errors' => array(array('There is something wrong, please check your input')),
                        'cabangName' => $name
                    );
                    $this->f3->set('SESSION.flash', $flash);
                    $this->f3->reroute('/cabang/input');
                }
            } catch (Exception $ex) {
                $flash = array(
                    'errorType' => 'Error(s)',
                    'errors' => array(array('There is something wrong, please check your input')),
                    'cabangName' => $name
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/cabang/input');
            }
        } else {
            $flash = array(
                'errorType' => 'Error(s)',
                'errors' => $v->errors(),
                'areaName' => $name,
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/cabang/input');
        }
    }

    public function edit() {
        $id = $this->f3->get('PARAMS.id');
        $v = new Valitron\Validator(array('Wilayah ID' => $id));
        $v->rule('required', ['Wilayah ID']);
        if ($v->validate()) {
            $wilayah = $this->wilayahSvr->findOne($this->session->sessionId,$id);
            $this->f3->set('wilayah', $wilayah);
            $this->f3->set('view', 'wilayah/edit.html');
        } else {
            $this->f3->reroute('/wilayah');
        }
    }

    public function update() {

    }

}
