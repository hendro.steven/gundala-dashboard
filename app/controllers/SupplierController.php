<?php


class SupplierController extends RestrictedController {

    public function index() {
        $supplier = new Supplier($this->db);
        $suppliers = $supplier->find();
        $suppliers = array_map(array($supplier,'cast'),(array)$suppliers,array());
        $this->f3->set('suppliers', $suppliers);
        $this->f3->set('view', 'suppliers/list.html');
    }

    public function input() {
        $this->f3->set('view', 'suppliers/input.html');
    }

    public function save(){
        $name = $this->f3->get('POST.supplierName');
        $email = $this->f3->get('POST.supplierEmail');
        $currency = $this->f3->get('POST.supplierCurrency');
        $address = $this->f3->get('POST.supplierAddress');
        $city = $this->f3->get('POST.supplierCity');
        $country = $this->f3->get('POST.supplierCountry');
        $zipcode = $this->f3->get('POST.supplierZip');
        $npwp = $this->f3->get('POST.supplierNpwp');
        $tax = $this->f3->get('POST.supplierTax');
        $nppkp = $this->f3->get('POST.supplierNppkp');
        $director = $this->f3->get('POST.supplierDirector');
        $npwpName = $this->f3->get('POST.supplierNpwpName');
        $npwpAddress = $this->f3->get('POST.supplierNpwpAddress');
        $nik = $this->f3->get('POST.supplierNik');

        $v = new Valitron\Validator(array('Supplier Name' => $name,'Supplier Email'=>$email));
        $v->rule('required', ['Supplier Name', 'Supplier Email']);
        $v->rule('email',['Supplier Email']);

        if ($v->validate()) {
            $supplier = new Supplier($this->db);
            $supplier->name = $name;
            $supplier->email = $email;
            $supplier->status = 1;
            $supplier->currency = $currency;
            $supplier->address = $address;
            $supplier->city = $city;
            $supplier->country = $country;
            $supplier->zipcode = $zipcode;
            $supplier->npwp = $npwp;
            $supplier->tax = $tax;
            $supplier->nppkp = $nppkp;
            $supplier->director_name = $director;
            $supplier->npwp_name = $npwp;
            $supplier->npwp_address = $npwpAddress;
            $supplier->nik = $nik;

            $date = date('Y-m-d H:i:s');
            $account = $this->f3->get('SESSION.acc');

            $supplier->created_at = $date;
            $supplier->created_by = $account;
            $supplier->updated_at = $date;
            $supplier->updated_by = $account;
            
            try {
                $supplier->save();
                $flash = array(
                    'errorType' => 'Success',
                    'infos' => array(array('Supplier saved'))
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/suppliers');
            } catch (Exception $ex) {
                $flash = array(
                    'errorType' => 'Error(s)',
                    'errors' => array(array('There is something wrong, please check your input')),
                    'supplierName' => $name,
                    'supplierEmail' => $email,
                    'supplierCurrency' => $currency,
                    'supplierAddress' => $address,
                    'supplierCity' => $city,
                    'supplierCountry' => $country,
                    'supplierZip' => $zipcode,
                    'supplierNpwp'=> $npwp,
                    'supplierTax' => $tax,
                    'supplierNppkp' => $nppkp,
                    'supplierDirector' => $director,
                    'supplierNpwpName' => $npwpName,
                    'supplierNpwpAddress' => $npwpAddress,
                    'supplierNik' => $nik
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/suppliers/input');
            }
        } else {
            $flash = array(
                'errorType' => 'Error(s)',
                'errors' => $v->errors(),
                'supplierName' => $name,
                'supplierEmail' => $email,
                'supplierCurrency' => $currency,
                'supplierAddress' => $address,
                'supplierCity' => $city,
                'supplierCountry' => $country,
                'supplierZip' => $zipcode,
                'supplierNpwp'=> $npwp,
                'supplierTax' => $tax,
                'supplierNppkp' => $nppkp,
                'supplierDirector' => $director,
                'supplierNpwpName' => $npwpName,
                'supplierNpwpAddress' => $npwpAddress,
                'supplierNik' => $nik  
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/suppliers/input');
        }
    }

    public function remove() {
        $id = $this->f3->get('GET.id');
        $v = new Valitron\Validator(array('Supplier ID' => $id));
        $v->rule('required', ['Supplier ID']);
        if ($v->validate()) {
            try {
                $supplier = new Supplier($this->db);
                $supplier->load(array('_id=?',$id));
                $supplier->erase();               
                $flash = array(
                    'errorType' => 'Success',
                    'infos' => array(array('Supplier data deleted'))
                );
                $this->f3->set('SESSION.flash', $flash);
            } catch (Exception $ex) {
                $flash = array(
                    'errorType' => 'Error(s)',
                    'errors' => array(array('This Supplier can not be deleted because already connected with some datas'))
                );
                $this->f3->set('SESSION.flash', $flash);
            }
        }
        $this->f3->reroute('/suppliers');
    }

    public function edit() {
        $id = $this->f3->get('GET.id');
        $v = new Valitron\Validator(array('Supplier ID' => $id));
        $v->rule('required', ['Supplier ID']);
        if ($v->validate()) {
            $supplier = new Supplier($this->db);
            $suppliers = $supplier->find(array('_id=?',$id))[0];           
            $this->f3->set('supplier', $suppliers->cast());
            $this->f3->set('view', 'suppliers/edit.html');
        } else {
            $this->f3->reroute('/suppliers');
        }
    }

    public function update() {
        $id = $this->f3->get('POST.id');
        $status = $this->f3->get('POST.supplierStatus');
        $name = $this->f3->get('POST.supplierName');
        $email = $this->f3->get('POST.supplierEmail');
        $currency = $this->f3->get('POST.supplierCurrency');
        $address = $this->f3->get('POST.supplierAddress');
        $city = $this->f3->get('POST.supplierCity');
        $country = $this->f3->get('POST.supplierCountry');
        $zipcode = $this->f3->get('POST.supplierZip');
        $npwp = $this->f3->get('POST.supplierNpwp');
        $tax = $this->f3->get('POST.supplierTax');
        $nppkp = $this->f3->get('POST.supplierNppkp');
        $director = $this->f3->get('POST.supplierDirector');
        $npwpName = $this->f3->get('POST.supplierNpwpName');
        $npwpAddress = $this->f3->get('POST.supplierNpwpAddress');
        $nik = $this->f3->get('POST.supplierNik');

        $v = new Valitron\Validator(array('Supplier Name' => $name,'Supplier Email'=>$email));
        $v->rule('required', ['Supplier Name', 'Supplier Email']);
        $v->rule('email',['Supplier Email']);

        if ($v->validate()) {
            $supplier = new Supplier($this->db);
            $supplier->load(array('_id=?',$id));
            $supplier->name = $name;
            $supplier->email = $email;
            $supplier->status = (int)$status;
            $supplier->currency = $currency;
            $supplier->address = $address;
            $supplier->city = $city;
            $supplier->country = $country;
            $supplier->zipcode = $zipcode;
            $supplier->npwp = $npwp;
            $supplier->tax = $tax;
            $supplier->nppkp = $nppkp;
            $supplier->director_name = $director;
            $supplier->npwp_name = $npwp;
            $supplier->npwp_address = $npwpAddress;
            $supplier->nik = $nik;

            $date = date('Y-m-d H:i:s');
            $account = $this->f3->get('SESSION.acc');

            $supplier->updated_at = $date;
            $supplier->updated_by = $account;
            $supplier->save();

            $flash = array(
                'errorType' => 'Success',
                'infos' => array(array('Supplier data updated'))
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/suppliers');
        } else {
            $flash = array(
                'errorType' => 'Error(s)',
                'errors' => $v->errors()
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/suppliers/edit?id=' . $id);
        }
    }

}
