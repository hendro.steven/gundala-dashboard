<?php


class CurrencyRateController extends RestrictedController {

    public function index() {
        $rates = new CurrencyRate($this->db);
        $currencyRates = $rates->find();
        $currencyRates = array_map(array($rates,'cast'),(array)$currencyRates,array());
        $this->f3->set('currencyRates', $currencyRates);
        $this->f3->set('view', 'rates/list.html');
    }

    public function input() {
        $this->f3->set('view', 'rates/input.html');
    }

    public function save(){
        $code = $this->f3->get('POST.currencyCode');
        $value = $this->f3->get('POST.currencyValue');

        $v = new Valitron\Validator(array('Currency Code' => $code,'Currency Value'=>$value));
        $v->rule('required', ['Currency Code', 'Currency Value']);
        $v->rule('numeric',['Currency Value']);

        if ($v->validate()) {
            $rates = new CurrencyRate($this->db);
            $rates->code = $code;
            $rates->value = $value;
            $date = date('Y-m-d H:i:s');
            $account = $this->f3->get('SESSION.acc');           
            $rates->created_at = $date;
            $rates->created_by = $account;
            $rates->updated_at = $date;
            $rates->updated_by = $account;
            
            try {
                $rates->save();
                $flash = array(
                    'errorType' => 'Success',
                    'infos' => array(array('Currency rates saved'))
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/rates');
            } catch (Exception $ex) {
                $flash = array(
                    'errorType' => 'Error(s)',
                    'errors' => array(array('There is something wrong, please check your input')),
                    'currencyCode' => $code,
                    'currencyValue' => $value                   
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/rates/input');
            }
        } else {
            $flash = array(
                'errorType' => 'Error(s)',
                'errors' => $v->errors(),
                'currencyCode' => $code,
                'currencyValue' => $value       
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/rates/input');
        }
    }

    public function remove() {
        $id = $this->f3->get('GET.id');
        $v = new Valitron\Validator(array('Rate ID' => $id));
        $v->rule('required', ['Rate ID']);
        if ($v->validate()) {
            try {
                $rate = new CurrencyRate($this->db);
                $rate->load(array('_id=?',$id));
                $rate->erase();               
                $flash = array(
                    'errorType' => 'Success',
                    'infos' => array(array('Currency rate data deleted'))
                );
                $this->f3->set('SESSION.flash', $flash);
            } catch (Exception $ex) {
                $flash = array(
                    'errorType' => 'Error(s)',
                    'errors' => array(array('This Currency rate can not be deleted because it already connected with some datas'))
                );
                $this->f3->set('SESSION.flash', $flash);
            }
        }
        $this->f3->reroute('/rates');
    }

    public function edit() {
        $id = $this->f3->get('GET.id');
        $v = new Valitron\Validator(array('Rate ID' => $id));
        $v->rule('required', ['Rate ID']);
        if ($v->validate()) {
            $rateModel = new CurrencyRate($this->db);
            $rate = $rateModel->find(array('_id=?',$id))[0];           
            $this->f3->set('rate', $rate->cast());
            $this->f3->set('view', 'rates/edit.html');
        } else {
            $this->f3->reroute('/rates');
        }
    }

    public function update() {
        $id = $this->f3->get('POST.id');
        $code = $this->f3->get('POST.currencyCode');
        $value = $this->f3->get('POST.currencyValue');

        $v = new Valitron\Validator(array('Currency Code' => $code,'Currency Value'=>$value));
        $v->rule('required', ['Currency Code', 'Currency Value']);
        $v->rule('numeric',['Currency Value']);

        if ($v->validate()) {
            $rate = new CurrencyRate($this->db);
            $rate->load(array('_id=?',$id));
            $rate->code = $code;
            $rate->value = $value;
            $date = date('Y-m-d H:i:s');
            $account = $this->f3->get('SESSION.acc');                    
            $rate->updated_at = $date;
            $rate->updated_by = $account;
            $rate->save();

            $flash = array(
                'errorType' => 'Success',
                'infos' => array(array('Currency rate data updated'))
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/rates');
        } else {
            $flash = array(
                'errorType' => 'Error(s)',
                'errors' => $v->errors()
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/rates/edit?id=' . $id);
        }
    }

}
