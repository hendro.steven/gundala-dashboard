<?php


class ServiceUrlController extends RestrictedController {

    public function index() {
        $serviceUrl = new ServiceUrl($this->db);
        $services = $serviceUrl->find();
        $services = array_map(array($serviceUrl,'cast'),(array)$services,array());
        $this->f3->set('services', $services);
        $this->f3->set('view', 'serviceurl/list.html');
    }

    public function input() {
        $this->f3->set('view', 'serviceurl/input.html');
    }

    public function save(){
        $name = $this->f3->get('POST.serviceName');
        $url = $this->f3->get('POST.serviceUrl');
        $token = $this->f3->get('POST.serviceToken');
        $description = $this->f3->get('POST.serviceDescription');

        $v = new Valitron\Validator(array('Service Name' => $name,'Service Url'=>$url));
        $v->rule('required', ['Service Name', 'Service Url']);
        $v->rule('url',['Service Url']);

        if ($v->validate()) {
            $serviceUrl = new ServiceUrl($this->db);
            $serviceUrl->name = $name;
            $serviceUrl->url = $url;
            $serviceUrl->token = $token;
            $serviceUrl->description = $description;
            $date = date('Y-m-d H:i:s');
            $account = $this->f3->get('SESSION.acc');
            $serviceUrl->created_at = $date;
            $serviceUrl->created_by = $account;
            $serviceUrl->updated_at = $date;
            $serviceUrl->updated_by = $account;
            
            try {
                $serviceUrl->save();
                $flash = array(
                    'errorType' => 'Success',
                    'infos' => array(array('Service Url saved'))
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/servicesurl');
            } catch (Exception $ex) {
                $flash = array(
                    'errorType' => 'Error(s)',
                    'errors' => array(array('There is something wrong, please check your input')),
                    'serviceName' => $name,
                    'serviceUrl' => $url,
                    'serviceToken' => $token,
                    'serviceDescription' => $description
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/servicesurl/input');
            }
        } else {
            $flash = array(
                'errorType' => 'Error(s)',
                'errors' => $v->errors(),
                'serviceName' => $name,
                'serviceUrl' => $url,
                'serviceToken' => $token,
                'serviceDescription' => $description
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/servicesurl/input');
        }
    }

    public function remove() {
        $id = $this->f3->get('GET.id');
        $v = new Valitron\Validator(array('Service ID' => $id));
        $v->rule('required', ['Service ID']);
        if ($v->validate()) {
            try {
                $serviceUrl = new ServiceUrl($this->db);
                $serviceUrl->load(array('_id=?',$id));
                $serviceUrl->erase();               
                $flash = array(
                    'errorType' => 'Success',
                    'infos' => array(array('Service data deleted'))
                );
                $this->f3->set('SESSION.flash', $flash);
            } catch (Exception $ex) {
                $flash = array(
                    'errorType' => 'Error(s)',
                    'errors' => array(array('This Service can not be deleted because it already connected with some datas'))
                );
                $this->f3->set('SESSION.flash', $flash);
            }
        }
        $this->f3->reroute('/servicesurl');
    }

    public function edit() {
        $id = $this->f3->get('GET.id');
        $v = new Valitron\Validator(array('Service ID' => $id));
        $v->rule('required', ['Service ID']);
        if ($v->validate()) {
            $serviceUrl = new ServiceUrl($this->db);
            $service = $serviceUrl->find(array('_id=?',$id))[0];
            $this->logger->write(json_encode($service->cast()));
            $this->f3->set('service', $service->cast());
            $this->f3->set('view', 'serviceurl/edit.html');
        } else {
            $this->f3->reroute('/servicesurl');
        }
    }

    public function update() {
        $id = $this->f3->get('POST.id');
        $name = $this->f3->get('POST.serviceName');
        $url = $this->f3->get('POST.serviceUrl');
        $token = $this->f3->get('POST.serviceToken');
        $description = $this->f3->get('POST.serviceDescription');

        $v = new Valitron\Validator(array('ID'=>$id,'Service Name' => $name,'Service Url'=>$url));
        $v->rule('required', ['ID','Service Name', 'Service Url']);
        $v->rule('url',['Service Url']);

        if ($v->validate()) {
            $serviceUrl = new ServiceUrl($this->db);
            $serviceUrl->load(array('_id=?',$id));
            $serviceUrl->name = $name;
            $serviceUrl->url = $url;
            $serviceUrl->token = $token;
            $serviceUrl->description = $description;
            $date = date('Y-m-d H:i:s');
            $account = $this->f3->get('SESSION.acc');           
            $serviceUrl->updated_at = $date;
            $serviceUrl->updated_by = $account;
            $serviceUrl->save();

            $flash = array(
                'errorType' => 'Success',
                'infos' => array(array('Service data updated'))
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/servicesurl');
        } else {
            $flash = array(
                'errorType' => 'Error(s)',
                'errors' => $v->errors()
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/servicesurl/edit?id=' . $id);
        }
    }

}
