<?php


class SurveyorController extends RestrictedController {

    private $cabangSvr;
    private $surveyorSvr;

    public function __construct() {
        parent::__construct(); 
        $this->surveyorSvr = new SurveyorServices();
        $this->cabangSvr = new CabangServices();
    }

    public function index() {
        $listSurveyor = $this->surveyorSvr->findAll($this->session->sessionId);
        $this->f3->set('listSurveyor',json_decode($listSurveyor,true));
        $this->f3->set('view', 'surveyor/list.html');
    }

    public function input() {
        $listCabang = $this->cabangSvr->findAll($this->session->sessionId);
        $this->f3->set('listCabang',json_decode($listCabang,true));
        $this->f3->set('view', 'surveyor/input.html');    
    }

    public function save(){
        $name = $this->f3->get('POST.surveyorName');
        $email = $this->f3->get('POST.surveyorEmail');
        $phone = $this->f3->get('POST.surveyorPhone');
        $password = $this->f3->get('POST.password');
        $cabang = $this->f3->get('POST.cabang');

        $v = new Valitron\Validator(array('Name' => $name, 'Email'=>$email, 'Phone'=>$phone,'Password'=>$password));
        $v->rule('required', ['Name', 'Email', 'Phone', 'Password']);
        $v->rule('email',['Email']);

        if ($v->validate()) {
            try {
                $result = $this->surveyorSvr->createOne($this->session->sessionId, $email, $phone, $password, $name, $cabang);
                $this->logger->write(json_encode($result));
                if($result->status){
                    $flash = array(
                        'errorType' => 'Success',
                        'infos' => array(array('Surveyor saved'))
                    );
                    $this->f3->set('SESSION.flash', $flash);
                    $this->f3->reroute('/surveyor');
                }else{
                    $flash = array(
                        'errorType' => 'Error(s)',
                        'errors' => array(array('There is something wrong, please check your input')),
                        'surveyorName' => $name,
                        'surveyorEmail' => $email,
                        'surveyorPhone' => $phone
                    );
                    $this->f3->set('SESSION.flash', $flash);
                    $this->f3->reroute('/surveyor/input');
                }
            } catch (Exception $ex) {
                $flash = array(
                    'errorType' => 'Error(s)',
                    'errors' => array(array('There is something wrong, please check your input')),
                    'surveyorName' => $name,
                    'surveyorEmail' => $email,
                    'surveyorPhone' => $phone
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/surveyor/input');
            }
        } else {
            $flash = array(
                'errorType' => 'Error(s)',
                'errors' => $v->errors(),
                'surveyorName' => $name,
                'surveyorEmail' => $email,
                'surveyorPhone' => $phone
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/surveyor/input');
        }
    }

    public function edit() {
        $id = $this->f3->get('PARAMS.id');
        $v = new Valitron\Validator(array('Wilayah ID' => $id));
        $v->rule('required', ['Wilayah ID']);
        if ($v->validate()) {
            $wilayah = $this->wilayahSvr->findOne($this->session->sessionId,$id);
            $this->f3->set('wilayah', $wilayah);
            $this->f3->set('view', 'wilayah/edit.html');
        } else {
            $this->f3->reroute('/wilayah');
        }
    }

    public function update() {

    }

}
