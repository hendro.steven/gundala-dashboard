<?php


class DealerController extends RestrictedController {

    private $dealerSvr;

    public function __construct() {
        parent::__construct(); 
        $this->dealerSvr = new DealerServices();
    }

    public function index() {
        $listDealer = $this->dealerSvr->findAll($this->session->sessionId);
        $this->f3->set('listDealer',json_decode($listDealer,true));
        $this->f3->set('view', 'dealer/list.html');
    }

    public function input() {       
        $this->f3->set('view', 'dealer/input.html');    
    }

    public function save(){
        $name = $this->f3->get('POST.dealerName');
        $contact = $this->f3->get('POST.dealerContact');
        $email = $this->f3->get('POST.dealerEmail');
        $phone = $this->f3->get('POST.dealerPhone');
        $address = $this->f3->get('POST.dealerAddress');
        $password = $this->f3->get('POST.password');

        $v = new Valitron\Validator(array('Name' => $name,'Contact'=>$contact, 'Email'=>$email, 'Phone'=>$phone,
                'Address'=>$address,'Password'=>$password));
        $v->rule('required', ['Name', 'Contact', 'Email','Phone','Address', 'Password']);
        $v->rule('email',['Email']);

        if ($v->validate()) {
            try {
                $result = $this->dealerSvr->createOne($this->session->sessionId, $email, $phone, $password, $contact, $name, $address);
                $this->logger->write(json_encode($result));
                if($result->status){
                    $flash = array(
                        'errorType' => 'Success',
                        'infos' => array(array('Dealer saved'))
                    );
                    $this->f3->set('SESSION.flash', $flash);
                    $this->f3->reroute('/dealer');
                }else{
                    $flash = array(
                        'errorType' => 'Error(s)',
                        'errors' => array(array('There is something wrong, please check your input')),                       
                        'dealerName' => $name,
                        'dealerContact' => $contact,
                        'dealerEmail' => $email,
                        'dealerPhone' => $phone,
                        'dealerAddress' => $address
                    );
                    $this->f3->set('SESSION.flash', $flash);
                    $this->f3->reroute('/dealer/input');
                }
            } catch (Exception $ex) {
                $flash = array(
                    'errorType' => 'Error(s)',
                    'errors' => array(array('There is something wrong, please check your input')),                   
                    'dealerName' => $name,
                    'dealerContact' => $contact,
                    'dealerEmail' => $email,
                    'dealerPhone' => $phone,
                    'dealerAddress' => $address
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/dealer/input');
            }
        } else {
            $flash = array(
                'errorType' => 'Error(s)',
                'errors' => $v->errors(),              
                'dealerName' => $name,
                'dealerContact' => $contact,
                'dealerEmail' => $email,
                'dealerPhone' => $phone,
                'dealerAddress' => $address
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/dealer/input');
        }
    }

    public function edit() {
        $id = $this->f3->get('PARAMS.id');
        $v = new Valitron\Validator(array('Wilayah ID' => $id));
        $v->rule('required', ['Wilayah ID']);
        if ($v->validate()) {
            $wilayah = $this->wilayahSvr->findOne($this->session->sessionId,$id);
            $this->f3->set('wilayah', $wilayah);
            $this->f3->set('view', 'wilayah/edit.html');
        } else {
            $this->f3->reroute('/wilayah');
        }
    }

    public function update() {

    }

}
