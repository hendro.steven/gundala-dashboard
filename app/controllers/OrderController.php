<?php


class OrderController extends RestrictedController {

    private $orderSvr;
    private $surveyorSvr;

    public function __construct() {
        parent::__construct(); 
        $this->orderSvr = new OrderServices();
        $this->surveyorSvr = new SurveyorServices();
    }

    public function index() {
        $listApp = $this->orderSvr->findAll($this->session->sessionId);
        $this->f3->set('listApp',json_decode($listApp,true));
        $this->f3->set('view', 'order/list.html');
    }


    public function detail() {
        $id = $this->f3->get('PARAMS.id');
        $v = new Valitron\Validator(array('Order ID' => $id));
        $v->rule('required', ['Order ID']);
        if ($v->validate()) {
            $order = $this->orderSvr->findOne($this->session->sessionId,$id);
            $documents = $this->orderSvr->getImages($id);
            $documents = json_decode($documents,true);
            $this->logger->write(json_encode($documents));

            $this->f3->set('application', $order);
            $this->f3->set('documents', $documents);
            $listSurveyor = json_decode($this->surveyorSvr->findAll($this->session->sessionId),true);
            $this->f3->set('listSurveyor', $listSurveyor);
            $this->f3->set('view', 'order/detail.html');
        } else {
            $this->f3->reroute('/order');
        }
    }

    public function update() {
        $id = $this->f3->get('POST.id');
        $button = $this->f3->get('POST.button');
        if($button == 'update'){
            $surveyor = $this->f3->get('POST.surveyor');
            $this->appSvr->updateSurveyor($this->session->sessionId, $id, $surveyor);
            $this->appSvr->updateStatus($this->session->sessionId, $id, $this->session->account->_id, "On Process");
        }else{
            $this->appSvr->updateStatus($this->session->sessionId, $id, $this->session->account->_id, $button);
        }     
        $this->f3->reroute("/order/$id");
    }

    

}
