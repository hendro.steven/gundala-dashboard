<?php


class AreaController extends RestrictedController {

    private $areaSvr;
    private $wilayahSvr;

    public function __construct() {
        parent::__construct(); 
        $this->areaSvr = new AreaServices();
        $this->wilayahSvr = new WilayahServices();
    }

    public function index() {
        $listArea = $this->areaSvr->findAll($this->session->sessionId);
        $this->f3->set('listArea',json_decode($listArea,true));
        $this->f3->set('view', 'area/list.html');
    }

    public function input() {
        $listWilayah = $this->wilayahSvr->findAll($this->session->sessionId);
        $this->f3->set('listWilayah',json_decode($listWilayah,true));
        $this->f3->set('view', 'area/input.html');    
    }

    public function save(){
        $name = $this->f3->get('POST.areaName');
        $wilayah = $this->f3->get('POST.wilayah');

        $v = new Valitron\Validator(array('Area Name' => $name));
        $v->rule('required', ['Area Name']);

        if ($v->validate()) {
            try {
                $this->areaSvr->createOne($this->session->sessionId, $name, $wilayah);
                $flash = array(
                    'errorType' => 'Success',
                    'infos' => array(array('Area saved'))
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/area');
            } catch (Exception $ex) {
                $flash = array(
                    'errorType' => 'Error(s)',
                    'errors' => array(array('There is something wrong, please check your input')),
                    'areaName' => $name
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/area/input');
            }
        } else {
            $flash = array(
                'errorType' => 'Error(s)',
                'errors' => $v->errors(),
                'areaName' => $name,
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/area/input');
        }
    }

    public function edit() {
        $id = $this->f3->get('PARAMS.id');
        $v = new Valitron\Validator(array('Wilayah ID' => $id));
        $v->rule('required', ['Wilayah ID']);
        if ($v->validate()) {
            $wilayah = $this->wilayahSvr->findOne($this->session->sessionId,$id);
            $this->f3->set('wilayah', $wilayah);
            $this->f3->set('view', 'wilayah/edit.html');
        } else {
            $this->f3->reroute('/wilayah');
        }
    }

    public function update() {

    }

}
