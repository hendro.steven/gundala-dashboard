<?php


class WilayahController extends RestrictedController {

    private $wilayahSvr;

    public function __construct() {
        parent::__construct(); 
        $this->wilayahSvr = new WilayahServices();
    }

    public function index() {
        $listWilayah = $this->wilayahSvr->findAll($this->session->sessionId);
        $this->f3->set('listWilayah',json_decode($listWilayah,true));
        $this->f3->set('view', 'wilayah/list.html');
    }

    public function input() {
        $this->f3->set('view', 'wilayah/input.html');    
    }

    public function save(){
        $name = $this->f3->get('POST.wilayahName');

        $v = new Valitron\Validator(array('Wilayah Name' => $name));
        $v->rule('required', ['Wilayah Name']);

        if ($v->validate()) {
            try {
                $this->wilayahSvr->createOne($this->session->sessionId, $name);
                $flash = array(
                    'errorType' => 'Success',
                    'infos' => array(array('Wilayah saved'))
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/wilayah');
            } catch (Exception $ex) {
                $flash = array(
                    'errorType' => 'Error(s)',
                    'errors' => array(array('There is something wrong, please check your input')),
                    'wilayahName' => $name
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/wilayah/input');
            }
        } else {
            $flash = array(
                'errorType' => 'Error(s)',
                'errors' => $v->errors(),
                'wilayahName' => $name,
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/wilayah/input');
        }
    }

    public function edit() {
        $id = $this->f3->get('PARAMS.id');
        $v = new Valitron\Validator(array('Wilayah ID' => $id));
        $v->rule('required', ['Wilayah ID']);
        if ($v->validate()) {
            $wilayah = $this->wilayahSvr->findOne($this->session->sessionId,$id);
            $this->f3->set('wilayah', $wilayah);
            $this->f3->set('view', 'wilayah/edit.html');
        } else {
            $this->f3->reroute('/wilayah');
        }
    }

    public function update() {

    }

}
