<?php


class BrandController extends RestrictedController {

    public function index() {
        $brand = new Brand($this->db);
        $brands = $brand->find();
        $brands = array_map(array($brand,'cast'),(array)$brands,array());  
        if($brands[0]){
            $this->f3->set('brands', $brands);
        }else{
            $this->f3->set('brands', []);
        }
        $this->f3->set('view', 'brands/list.html');
    }

    public function input() {
        $supplier = new Supplier($this->db);
        $suppliers = $supplier->find();
        $suppliers = array_map(array($supplier,'cast'),(array)$suppliers,array());
        $this->f3->set('suppliers', $suppliers);
        $this->f3->set('view', 'brands/input.html');
    }

    public function save(){
        $name = $this->f3->get('POST.brandName');
        $identifier = $this->f3->get('POST.brandIdentifier');
        $supplierId = $this->f3->get('POST.brandSupplier');
        $merchandiser = $this->f3->get('POST.brandMerchandiser');
        
        $v = new Valitron\Validator(array('Brand Name' => $name,'Identifier'=>$identifier, 'Supplier'=>$supplierId, 'Merchandiser'=>$merchandiser));
        $v->rule('required', ['Brand Name', 'Identifier','Supplier','Merchandiser']);

        if ($v->validate()) {
            $supplier = new Supplier($this->db);
            $suppliers = $supplier->find(array('_id', $supplierId))[0];
            $brand = new Brand($this->db);
            $brand->name = $name;
            $brand->identifier = $identifier;
            $brand->supplier = $suppliers->cast();
            $brand->merchandiser = $merchandiser;
           
            $date = date('Y-m-d H:i:s');
            $account = $this->f3->get('SESSION.acc');

            $brand->created_at = $date;
            $brand->created_by = $account;
            $brand->updated_at = $date;
            $brand->updated_by = $account;
            
            try {
                $brand->save();
                $flash = array(
                    'errorType' => 'Success',
                    'infos' => array(array('Brand saved'))
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/brands');
            } catch (Exception $ex) {
                $flash = array(
                    'errorType' => 'Error(s)',
                    'errors' => array(array('There is something wrong, please check your input')),
                    'brandName' => $name,
                    'brandIdentifier' => $identifier,
                    'brandMerchandiser' => $merchandiser,
                );
                $this->f3->set('SESSION.flash', $flash);
                $this->f3->reroute('/brands/input');
            }
        } else {
            $flash = array(
                'errorType' => 'Error(s)',
                'errors' => $v->errors(),
                'brandName' => $name,
                'brandIdentifier' => $identifier,
                'brandMerchandiser' => $merchandiser,  
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/brands/input');
        }
    }

    public function remove() {
        $id = $this->f3->get('GET.id');
        $v = new Valitron\Validator(array('Brand ID' => $id));
        $v->rule('required', ['Brand ID']);
        if ($v->validate()) {
            try {
                $brand = new Brand($this->db);
                $brand->load(array('_id=?',$id));
                $brand->erase();               
                $flash = array(
                    'errorType' => 'Success',
                    'infos' => array(array('Brand data deleted'))
                );
                $this->f3->set('SESSION.flash', $flash);
            } catch (Exception $ex) {
                $flash = array(
                    'errorType' => 'Error(s)',
                    'errors' => array(array('This Brand can not be deleted because already connected with some datas'))
                );
                $this->f3->set('SESSION.flash', $flash);
            }
        }
        $this->f3->reroute('/brands');
    }

    public function edit() {
        $id = $this->f3->get('GET.id');
        $v = new Valitron\Validator(array('Brand ID' => $id));
        $v->rule('required', ['Brand ID']);
        if ($v->validate()) {
            $supplier = new Supplier($this->db);
            $suppliers = $supplier->find();
            $suppliers = array_map(array($supplier,'cast'),(array)$suppliers,array());
            $this->f3->set('suppliers', $suppliers);
            $brand = new Brand($this->db);
            $brands = $brand->find(array('_id=?',$id))[0];           
            $this->f3->set('brand', $brands->cast());
            $this->f3->set('view', 'brands/edit.html');
        } else {
            $this->f3->reroute('/brands');
        }
    }

    public function update() {
        $id = $this->f3->get('POST.id');
        $status = $this->f3->get('POST.brandStatus');
        $name = $this->f3->get('POST.brandName');
        $identifier = $this->f3->get('POST.brandIdentifier');
        $supplierId = $this->f3->get('POST.brandSupplier');
        $merchandiser = $this->f3->get('POST.brandMerchandiser');

        $v = new Valitron\Validator(array('Brand Name' => $name,'Identifier'=>$identifier, 'Supplier'=>$supplierId, 'Merchandiser'=>$merchandiser));
        $v->rule('required', ['Brand Name', 'Identifier','Supplier','Merchandiser']);

        if ($v->validate()) {
            $supplier = new Supplier($this->db);
            $suppliers = $supplier->find(array('_id', $supplierId))[0];
            $brand = new Brand($this->db);
            $brand->load(array('_id=?',$id));
            $brand->name = $name;
            $brand->status = (int)$status;
            $brand->identifier = $identifier;           
            $brand->supplier = $suppliers->cast();
            $brand->merchandiser = $merchandiser;
            
            $date = date('Y-m-d H:i:s');
            $account = $this->f3->get('SESSION.acc');

            $brand->updated_at = $date;
            $brand->updated_by = $account;
            $brand->save();

            $flash = array(
                'errorType' => 'Success',
                'infos' => array(array('Brand data updated'))
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/brands');
        } else {
            $flash = array(
                'errorType' => 'Error(s)',
                'errors' => $v->errors()
            );
            $this->f3->set('SESSION.flash', $flash);
            $this->f3->reroute('/brands/edit?id=' . $id);
        }
    }

}
