<?php

class AccountController extends Controller {

    private $loginSvr;

    function __construct(){
        parent::__construct();
        $this->loginSvr = new LoginServices();
    }

    public function signout() {
        $this->f3->clear('SESSION');
        $this->f3->reroute('/');
    }

    public function signin() {
        $email = $this->f3->get('POST.email');
        $password = $this->f3->get('POST.password');

        $v = new Valitron\Validator(array('Email' => $email, 'Password' => $password));
        $v->rule('required', ['Email', 'Password']);
        $v->rule('email', 'Email');

        if ($v->validate()) {
            $result = $this->loginSvr->login($email, $password);    
            $this->logger->write(json_encode($result));
            if ($result->data->valid) {
                $this->logger->write(json_encode($result->data->account->role));
                if($result->data->account->role == "ADMIN" || $result->data->account->role == "SUPER_ADMIN"){
                    $this->logger->write(json_encode("Login sukses"));
                    $this->f3->set('SESSION.acc', $result->data);
                    $this->f3->reroute('/dashboard');    
                }else{
                    $this->f3->set('email', $email);
                    $this->f3->set('errors', array(array('Login gagal, anda tidak berhak mengakses dashboard ini')));
                    echo Template::instance()->render('index.html');
                }                                       
            } else {
                $this->f3->set('email', $email);
                $this->f3->set('errors', array(array('Login gagal, wrong username or password')));
                echo Template::instance()->render('index.html');
            }
        } else {
            $this->f3->set('email', $email);
            $this->f3->set('errors', $v->errors());
            echo Template::instance()->render('index.html');
        }
    }



}
